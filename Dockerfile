FROM php:7.2

#COPY php.ini /usr/local/etc/php/conf.d/

RUN apt-get update -y && apt-get install -y zip unzip git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN apt-get install -y libpng-dev && docker-php-ext-install gd

RUN docker-php-ext-install pdo mbstring pdo_mysql

RUN docker-php-ext-enable pdo pdo_mysql
#RUN docker-php-ext-pdo.ini

RUN \
    docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-configure mysqli --with-mysqli=mysqlnd \
    && docker-php-ext-install pdo_mysql

WORKDIR /app
COPY . /app
RUN composer install

CMD php artisan serve --host=0.0.0.0 --port=80
EXPOSE 80

#RUN apt-get update -y \
#      && apt-get install -y  software-properties-common \
#      && apt-get install -y python-software-properties \
#      && LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php -y \
#      && apt-get update -y \
      
# Install Packages
#RUN apt-get update && apt-get install -my \
# supervisor \
#  curl \
#  php7.0-curl \
#  php7.0-fpm \
#  php7.0-mysql \
#  php7.0-mcrypt \
#  php7.0-sqlite \
#  php7.0-xdebug \
#  php7.0-mbstring \
#  php7.0-apc

# Ensure that PHP7 FPM is run as root.
#RUN service php7.0-fpm start

#RUN docker-php-ext-install pdo mbstring
#RUN apt-get install -y libpng-dev && docker-php-ext-install gd